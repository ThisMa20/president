var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./model');
var crypto = require('password-hash');
var fs = require('fs');

Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

////////////////////////////////////////////////////////

app.use(express.static(__dirname));

////////////////////////////////////////////////////////

app.get("/", function(req, res){
    res.sendFile(__dirname + 'index.html');
})

////////////////////////////////////////////////////////

var partys = {}

var sockets_search_party = []

var party_id = '0'

var user_connected = []

/**
 * Object:USER = { id_user  : Integer,      Identifiant unique 
 *                 login    : String,       Pseudo unique 
 *                 password : String,       Mots de passe (Bientot hashé)
 *                 email    : String,       Email unique
 *                 verify   : Boolean,      Email validé 
 *                 score    : Integer,      Score de jeu
 *                 nb_party : Integer,      Nombre de partie déja joué
 *                 play_time: Integer,      Temps joué en minute
 *                 socket   : String}       Id de la socket en cours de l'utilisateur 
 */


/////////////////////////////////////////////////////////

var connected_login = function(login){
    for(var user of user_connected){
        if(user!=undefined && user.login === login){
            return user
        }
    }
    return false
}

var get_connected = function(user){
    if(user_connected[user.id_user]){
        return user_connected[user.id_user]
    }
    return user
}

var disconnect_me = function(id_me){
    delete user_connected[id_me]
}

var remove_friend = function(user, id_old_friend){
    var i = 0
    io.to(user.socket).emit('friend_remove',user.login)
    for(var id of user.friends){
        if(id==id_old_friend){
            user.friends.remove(i)
            return true
        }
        i++
    }
}

function twoDigit(n) {
    return (n < 10 ? '0' : '') + n
}

var serveur_log = function(action, user){
    let date = new Date(Date.now())
    let txt = date+' | '+action+' from '+user.login+' ID:'+user.id_user
    console.log(txt)

    /*fs.appendFile('logs/log_'+date.getFullYear().toString()+twoDigit(date.getMonth()+1)+twoDigit(date.getDay())+'.txt', '\n'+txt, function (err) {
        if (err) throw err;
    });*/
    
    fs.appendFile('app/logs/log_'+date.getFullYear().toString()+twoDigit(date.getMonth()+1)+twoDigit(date.getDay())+'.txt', '\n'+txt, function (err) {
        if (err) throw err;
    });
}


//////////////////////////////////////////////////////////

io.sockets.on('connection', function(socket){
    var me = false;

    console.log('New page')

    for(var user of user_connected){
        if(user!=undefined){
            socket.emit('new_usr',user);
        }
    }


    /**
     * Gère la déconnection de l'utilisateur : - supprime l'utilisateur du tableau des utilisateur connecté         socket.emit('dis_usr',me)
     *                                         - prévient les amis de l'utilisateur amis qu'il ses déconnecté       io.to(friend.socket).emit('goodbye_my_friend',me)
     * 
     * Utilisation : - l'utilisateur a fermé sa page
     *               - l'utilisateur a appuyé sur le bouton "déconnexion"
     * 
     */
    socket.on('disconnect', function(){
        if(!me){
           return false; 
        }

        if(me.page.w_party){
            let party_id = me.party
            if(partys[party_id]){
                if(partys[party_id].master!=me.id_user){
                    io.to(party_id).emit('player_leave', me)
                    partys[party_id].players.remove(partys[party_id].players.indexOf(me))
                    for(let socket of sockets_search_party){
                        io.to(socket).emit('update_join_party', party_id, partys[party_id].players.length)
                    }

                }else{
                    io.to(party_id).emit('master_leave', party_id)
                    me.page.w_party = false
    
                    delete partys[party_id]
    
                    for(let socket of sockets_search_party){
                        io.to(socket).emit('remove_join_party', party_id)
                    }
                }
            }
        }
        
        serveur_log('Disconnect', me)

        disconnect_me(me.id_user);

        db.get_users(me.friends, function(friends){
            for(var friend of friends){
                friend = get_connected(friend)
                if(friend.socket){
                    io.to(friend.socket).emit('goodbye_my_friend',me);
                }
            }
        })
        db.save_user([me.login, me.email, me.score, me.number_of_party, me.play_time, me.id_user])
        me=false
    });

    /**
     * Gère la connexion et l'initialisation de l'utilisateur : - vérification des données entré par l'utilisateur (Pseudo/Mots de Passe,
     *                                                            déja connecté)
     *                                                          - enregistrement des données de l'utilisateur dans la variable me
     *                                                            + .socket     id du socket de l'utilisateur
     *                                                            + .friends    tableau des id des amis de l'utilisateur
     *                                                          - ajout de l'utilisateur dans le tableau des utilisateurs connectés     io.socket.emit('new_usr',me)
     *                                                          - prévenir les amis connectés de l'utilisateur amis qu'il ses connecté
     *                                                          - initialisation de ses informations (amis, demande d'amis)
     *                                                          - redirection de l'utilisateur vers le home
     * 
     * Utilisation : - l'utilisateur tente de se connecter
     */
    socket.on('try_to_connect', function(user){

        if(connected_login(user.login)){
            socket.emit('connection_err',['Vous etes déja connecté'])
        }else if(user.login === '' || user.pwd === ''){
            socket.emit('connection_err',['Il faut remplir toutes les cases'])
        }else{
            db.get_user_by_login(user.login, function(res){
                if(res.sqlMessage){
                    socket.emit('db_err', res.sqlMessage)
                }else{
                    if(res){
                        if(get_connected(res).socket){
                            socket.emit('connection_err',['Vous etes déja connecté'])
                        }else{
                            let user_res = res
                            if(crypto.verify(user.pwd, user_res.password)){
                                me = {
                                    id_user : user_res.id_user,
                                    login : user_res.login,
                                    email : user_res.email,
                                    score : user_res.score,
                                    number_of_party : user_res.number_of_party,
                                    play_time : user_res.play_time,
                                    socket : socket.id,
                                    page : {account : false,
                                            friend : false,
                                            s_party : false,
                                            w_party : false,
                                            i_party : false
                                            },
                                    party : null
                                } 

                                user_connected[me.id_user]=me

                                db.get_ids_friend(me.id_user, function(ids_friend){
                                    if(ids_friend.sqlMessage){
                                        socket.emit('db_err', ids_friend.sqlMessage)
                                    }else{
                                        me.friends = ids_friend
                                        
                                        db.get_users(me.friends, function(friends){
                                            if(friends.sqlMessage){
                                                socket.emit('db_err', friends.sqlMessage)
                                            }else{
                                                for(var friend of friends){
                                                    friend = get_connected(friend)
                                                    if(friend.socket && friend.page.friend){
                                                        io.to(friend.socket).emit('hello_my_friend',me);
                                                    }
                                                    socket.emit('friend_load',friend)
                                                }
                                                
                                                db.get_requests(me.id_user, function(requests){
                                                    if(requests.sqlMessage){
                                                        socket.emit('db_err', requests.sqlMessage)
                                                    }else{
                                                        for(var requester of requests){
                                                            socket.emit('friend_request_notif')
                                                        }
                                                    }
                                                })
                                                serveur_log('Connection', me)

                                                socket.emit('connected',me);
                                            }
                                        })
                                    }
                                })
                            }else{
                                socket.emit('connection_err',["Mot de passe incorrect"])
                            }
                        }
                    }else{
                        socket.emit('connection_err',["Ce Pseudo n'existe pas"])
                    }
                }
            })
        }
    });

    socket.on('load_page_home', function(){
        if(me.page.s_party){
            sockets_search_party.remove(sockets_search_party.indexOf(me.socket))
        }

        me.page.friend = false
        me.page.account = false
        me.page.w_party = false
        me.page.i_party = false
        me.page.s_party = false

        serveur_log('Load home page', me)
    })

    socket.on('load_page_friend', function(){
        me.page.friend = true

        serveur_log('Load friend page', me)

        db.get_users(me.friends, function(friends){
            if(friends.sqlMessage){
                socket.emit('db_err', friends.sqlMessage)
            }else{
                for(var friend of friends){
                    friend = get_connected(friend)
                    
                    socket.emit('friend_load',friend)
                }

                db.get_requests(me.id_user, function(requests){
                    if(requests.sqlMessage){
                        socket.emit('db_err', requests.sqlMessage)
                    }else{
                        for(var requester of requests){
                            socket.emit('friend_request',requester)
                        }
                    }
                })
            }
        })
    })

    /**
     * Gère l'inscription de l'utilisateur : - Vérification des données : - login           => pas encore utilisé
     *                                                                    - email           => pas encore utilisé
     *                                                                    - mots de passe   => confirmé correctement / supperieur à 6 caractères
     *                                       - enregistrement des données
     * 
     * Utilisation : - l'utilisateur tente de s'inscrire
     */
    socket.on('register', function(user){
        var err_tab = []

        if (user.login === '' || user.email === '' || user.pwd1 ==='' || user.pwd2 === ''){
            socket.emit('register_err',['Il faut remplir toutes les cases'])
        }else{
            if ((user.pwd1).length < 7){
                err_tab.push('Le mot de passe doit contenir au moin 7 caratères')
            }
            if (user.pwd1 != user.pwd2){
                err_tab.push('La confirmation de mot de passe ne correspond pas')
            }
            db.login_already_used(user.login, function(res){
                if(res.sqlMessage){
                    socket.emit('db_err', res.sqlMessage)
                }else{
                    if(res){
                        err_tab.push('Pseudo déja utilisé')
                    }
                    db.email_already_used(user.email, function(res){
                        if(res.sqlMessage){
                            socket.emit('db_err', res.sqlMessage)
                        }else{
                            if(res){
                                err_tab.push('Email déja utilisé')
                            }

                            if(err_tab.length == 0){
                                db.new_register([user.login, user.email, crypto.generate(user.pwd1)], function(res){
                                    if(res.sqlMessage){
                                        socket.emit('db_err', res.sqlMessage)
                                    }else{
                                        serveur_log('Registering', user)
                                        socket.emit('registered')
                                    }
                                })
                            }else{
                                socket.emit('register_err',err_tab)
                            }
                        }
                    })
                }
            })
        }
    });

    socket.on('try_to_change', function(user){

        var err_tab = []

        if (user.login === '' || user.email === '' || user.pwd1 ==='' || user.pwd2 === ''){
            socket.emit('change_err',['Il faut remplir toutes les cases soit par les nouvelles ou les enciennes valeurs'])
        }else{
            if ((user.pwd1).length < 7){
                err_tab.push('Le mot de passe doit contenir au moin 7 caratères')
            }
            if (user.pwd1 != user.pwd2){
                err_tab.push('La confirmation de mot de passe ne correspond pas')
            }
            db.login_already_used_not_me(user.login, me.login, function(res){
                if(res){
                    err_tab.push('Pseudo déja utilisé')
                }
                db.email_already_used_not_me(user.email, me.email, function(res){
                    if(res){
                        err_tab.push('Email déja utilisé')
                    }

                    if(err_tab.length == 0){
                        db.change_user([user.login, user.email, crypto.generate(user.pwd1), me.id_user], function(res){
                            if(res.sqlMessage){
                                socket.emit('db_err', res.sqlMessage)
                            }else{
                                socket.emit('charge_err', ['Modifications ont été enregistrer elles ne seront pas visible par les autres utilisateurs avant leurs prochaine connexion'])
                                socket.emit('change', me)
                                me.login = user.login
                                me.email = user.email

                                serveur_log('Account modification', me)

                            }
                        })
                        
                    }else{
                        socket.emit('change_err',err_tab)
                    }
                })
            })
        }
    })

    /**
     * Gère la recherche d'amis : - Vérification du Pseudo entré par l'utilisateur (il existe? , demande déja effectué et déja amis )
     *                            - Envoie de la demande d'amis à l'utilisateur si il est en ligne
     *                            - Enregistre la demande d'amis
     * 
     * Utilisation : - l'utilisateur recherche un nouvel amis
     */
    socket.on('search',function(login){
        var err_tab = []

        serveur_log('Searching friend "'+login+'"', me)

        if(login === ''){
            socket.emit('search_err',['Il faut remplir le champ'])
        }else if(login === me.login){
            socket.emit('search_err',['Es tu schizophrénie ?'])
            err = true
        }else{ 
            db.get_search(login, me.id_user, function(res){
                if(res.sqlMessage){
                    socket.emit('db_err', res.sqlMessage)
                }else{
                    if(res){
                        for(var find of res){
                            if(me.friends.includes(find.id_user)){
                                socket.emit('search_err',['Vous etes déja amis avec '+find.login])
                            }else{
                                socket.emit('search_find',find)

                            }
                        }
                    }else{
                        socket.emit('search_err',["Ce pseudo ne correspond a aucun utilisateur"])
                    }
                }
            })
        }
    })

    /**
     * Enregiste et informe d'une nouvelle demande d'amis 
     * 
     * Utilisation : - lors de la recherhce d'amis
     * 
     */
    socket.on('request_send',function(requested){
        db.already_request(requested.id_user, me.id_user, function(res){
            if(res.sqlMessage){
                socket.emit('db_err', res.sqlMessage)
            }else{
                if(res===true){
                    socket.emit('search_err',['Demande déja envoyé ou recu avec '+requested.login])
                }else{
                    serveur_log('Sending friend request to '+requested.login, me)
                    var connect = get_connected(requested)
                    if(connect.socket && connect.page.friend){
                        io.to(connect.socket).emit('friend_request',me)
                    }else if (connect.socket){
                        io.to(connect.socket).emit('friend_request_notif')
                    }

                    db.new_request(connect.id_user, me.id_user, function(res){
                        if(res.sqlMessage){
                            socket.emit('db_err',res.sqlMessage)
                        }else{
                            socket.emit('request_sended', connect)
                        }
                    })
                }
            }
        })
        
    })

    /**
     * Enregistre et informe un nouvel ami
     * 
     * Utilisation : - demande d'amis accepté
     */

    socket.on('request_accept',function(user){
        
        db.new_friendship(me.id_user, user.id_user, function(res){
            if(res.sqlMessage){
                socket.emit('db_err', res.sqlMessage)
            }else{
                serveur_log('New friendship with '+user.login, me)
                me.friends.push(user.id_user)
                var connect = get_connected(user)
                if(connect.socket && connect.page.friend){
                    connect.friends.push(me.id_user)
                    io.to(connect.socket).emit('friend_load',me)
                }
                socket.emit('friend_load',connect)

                socket.emit('request_accepted',connect);
            }
        })
    })

    /**
     * Enregistrement de la supprétion d'une demande d'amis
     * 
     * Utilisation : - demande d'amis refusé
     */
    socket.on('request_refuse',function(user){
        db.delete_request(me.id_user, user.id_user, function(res){
            if(res.sqlMessage){
                socket.emit('db_err', res.sqlMessage)
            }else{
                socket.emit('request_refused', user)
                serveur_log('Request refuse to '+user.login, me)
            }
        })
    })

    /**
     * Enregistrement et mise a jour de l'affichage lors de la supprétion d'une demande d'amis
     * 
     * Utilisation : - Suppretion d'amiti
     */
    socket.on('friend_suppr',function(friend){
        var old_friend = get_connected(friend)
        if(old_friend.socket && old_friend.page.friend){
            remove_friend(old_friend,me.id_user)
        }

        remove_friend(me, old_friend.id_user)

        db.delete_friendship(me.id_user, old_friend.id_user, function(res){
            if(res.sqlMessage){
                socket.emit('db_err', res.sqlMessage)
            }else{
                socket.emit('friend_suppred', old_friend)
                serveur_log('Friendship deleted with '+friend.login, me)
            }
        })
    })

    socket.on('load_page_account', function(){
        me.page.account = true
        socket.emit('account_load', me)

        serveur_log('Load account page', me)
    })

    //////JOIN PARTY////////////////////////////////////////////////

    

    socket.on('load_join_party', function(){
        me.page.s_party = true
        let partys_w = {}
        for(let party_id in partys){
            if(!partys[party_id].en_cours){
                partys_w[party_id] = partys[party_id]
            }
        }
        socket.emit('load_join_party',partys_w)
        sockets_search_party.push(me.socket)

        serveur_log('Load join page', me)
    })

    socket.on('want_to_join', function(party_id){

        let party = partys[party_id]

        
            socket.join(party_id)

            serveur_log('Join party : '+party_id, me)

            io.to(party_id).emit('new_player_join', me)

            partys[party_id].players.push(me)

            me.party = party_id

            sockets_search_party.remove(sockets_search_party.indexOf(me.socket))

            socket.emit('load_waiting_room', party)

            me.page.w_party = true
            me.page.s_party = false

            for(let socket of sockets_search_party){
                io.to(socket).emit('update_join_party', party_id, party.players.length)
            }
        
        
    })

    socket.on('want_to_leave', function(party_id){
        if(partys[party_id]){
            if(partys[party_id].master!=me.id_user){
                io.to(party_id).emit('player_leave', me)
                partys[party_id].players.remove(partys[party_id].players.indexOf(me))
                for(let socket of sockets_search_party){
                    io.to(socket).emit('update_join_party', party_id, partys[party_id].players.length)
                }
            }else{
                io.to(party_id).emit('master_leave', party_id)
                me.page.w_party = false

                delete partys[party_id]

                for(let socket of sockets_search_party){
                    io.to(socket).emit('remove_join_party', party_id)
                }
            }
        }

        me.page.w_party = false
        me.party = null
        socket.leave(party_id)
    })

    /////////CREATE GAME//////////////////////////////////////////////////////////////////

    socket.on('rules', function(rules){
		var err = false
        var err_tab = []
        
        /*
		if (!rules.rules_selected.includes('rule_end2') && rules.rules_selected.includes('rule_put2_in_void'))
		{
			err_tab.push("Incompatibilité entre les règles 'Finir par un deux' et 'Interdiction de mettre un 2 dans le vide'")
            err = true
        } */   
		
		if(!err)
		{		
            party_id = (parseInt(party_id) + 1).toString()
            partys[party_id] = { num : party_id,
                                players : [me],
                                players_pass : [],
                                players_end : [],
                                rules : { 
                                    "carreMagique" : false, 
                                    "2toutSeul" : true,
                                },
                                pile : {
                                    round : [], 
                                    cards : [{value : 0, color : 'null'}], //carte de départ
                                    nb : 0, //simple = 1, paire = 2, triple = 3, carré = 4
                                    revo : false,
                                    tg : false,
                                    player : null, //indice du joueur qui doit joueur
                                },
                                role : {
                                    President : null,
                                    Vice_President : null,
                                    Vice_Trou : null,
                                    Trou : null,
                                },
                                master : me.id_user,
                                en_cours : false
                            }
            
            for (let rule in rules){
                partys[party_id].rules[rule] = true
            } 

            socket.join(party_id)

            me.party = party_id

            me.page.w_party = true

            serveur_log('Create a party', me)

            socket.emit('rules_accepted', partys[party_id].num, me)

            for(let socket of sockets_search_party){
                io.to(socket).emit('new_party', partys[party_id])
            }
        } else
        {
            console.log(err_tab)
        }
		
	
    });
    
    socket.on('load_friends_list_in_game', function(party_id){
        
        db.get_users(me.friends, function(friends){
            if(friends.sqlMessage){
                socket.emit('db_err', friends.sqlMessage)
            }else{
                for(var friend of friends){
                    friend = get_connected(friend)

                    if(friend.socket){
                        socket.emit('friend_load_to_invite_in_game',friend, party_id)
                    }
                }

            }
        })
    })

    socket.on('game_request_send',function(requested, party_id){
        
        io.to(requested.socket).emit('game_request', partys[party_id])
        
        serveur_log('Send request for party to '+requested.login, me)
    })

    socket.on('want_to_start', function(id_party){
        partys[party_id].players_pass = []
        partys[party_id].players_end = []
        partys[party_id].pile =  {
            round : [], 
            cards : [{value : 0, color : 'null'}], //carte de départ
            nb : 0, //simple = 1, paire = 2, triple = 3, carré = 4
            revo : false,
            tg : false,
            player : null, //indice du joueur qui doit joueur
        }

        let party = partys[id_party]
        if(party.players.length < 4){
            socket.emit('start_game_err', 'Pas encore assez de joueur il faut être minimum 4')
        }else{
            for(let socket of sockets_search_party){
                io.to(socket).emit('remove_join_party', party_id)
            }

            partys[id_party].en_cours = true
            
            var handsArray = distribution(party.players.length, id_party);

            var round = [], playersLogin = [];
            var i = 0;

            //Initialisation de la partie
            for (let player of partys[id_party].players){
                playersLogin.push(player.login);
                round.push({
                    login : player.login,
                    nb_card : handsArray[i].length
                })
                i++;
            }

            partys[id_party].pile.round = round
            io.in(partys[id_party].num).emit('init_party', playersLogin);
            io.in(partys[id_party].num).emit('init_round', partys[id_party].pile.round);

            //Distribution des cartes
            i = 0; 
            for (let player of partys[id_party].players){
                io.to(player.socket).emit('new_cards', handsArray[i]);
                i++;
            }
            //Le joueur avec la dame de coeur commence
            io.to(partys[id_party].players[partys[id_party].pile.player].socket).emit('your_turn', false);

        }
    })

    socket.on('pass', function(){
        var party_id = me.party;

        if(partys[party_id].pile.cards.length == 1){
            socket.emit('played', false, 'Tu ne peux pas passer dès le début')
            return;
        }

            if (partys[party_id].players_pass.length == partys[party_id].players.length-1){
                partys[party_id].pile.nb = 0;
                partys[party_id].players_pass = [];
                partys[party_id].pile.tg = false;
                if (!partys[party_id].pile.revo){
                    partys[party_id].pile.cards = [{value : 0, color : 'null'}];
                } else{
                    partys[party_id].pile.cards = [{value : 16, color : 'null'}];
                }
                io.in(partys[party_id].num).emit('remove_pile', 'Tout le monde a passé');
                socket.emit('played',true, 'A toi de jouer');
                io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('your_turn', partys[party_id].pile.tg);
            } 
            else if(partys[party_id].players_pass.length >= partys[party_id].players.length-partys[party_id].players_end.length){
                io.in(partys[party_id].num).emit('remove_pile', 'Tout le monde a passé');
                socket.emit('played',true);
                partys[party_id].players_pass = [];
                nextPlayer(party_id)
                partys[party_id].pile.nb = 0;
                partys[party_id].pile.tg = false;
                if (!partys[party_id].pile.revo){
                    partys[party_id].pile.cards = [{value : 0, color : 'null'}];
                } else{
                    partys[party_id].pile.cards = [{value : 16, color : 'null'}];
                }
                io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('your_turn', partys[party_id].pile.tg);
            } 
            else {
                partys[party_id].pile.tg = false;
                partys[party_id].players_pass.push(partys[party_id].players[partys[party_id].pile.player].id_user);
                io.in(partys[party_id].num).emit('other_played', {login : partys[party_id].players[partys[party_id].pile.player].login, nb : 0}, me.login+' passe');
                //Si le joueur decide de ne pas jouer, on passe au joueur suivant
                socket.emit('played',true);
                nextPlayer(party_id);
                io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('your_turn', partys[party_id].pile.tg);
            }

        
    })

    socket.on('play', function(data){
        var party_id = me.party
        

        //TEST
        //Si le joueur se prend un TaGueule et ne peut jouer
        if (data == null){
            socket.emit('played', true)
            io.in(partys[party_id].num).emit('other_played', {login : partys[party_id].players[partys[party_id].pile.player].login, nb : 0}, me.login+' ne peut pas jouer');
            partys[party_id].pile.tg = false;
            nextPlayer(party_id);
            io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('your_turn', partys[party_id].pile.tg);
            return; //fini la fonction
        }else if(data.cards.length==0){
            socket.emit('played', false, 'Petit bug tu peux réésayer')
            return;
        }
        //TEST
        //Si le joueur a passer une fois volontairement, ne peut rejouer que au prochain pli
        if (partys[party_id].players_pass.includes(data.id)){
            socket.emit('played', false, 'Tu as déja passé tu ne peux pas rejouer')
            return;
        }
        //TEST
        //Si le joueur a déjà fini
        if (partys[party_id].players_end.includes(data.id)){
            return;
        }

        
        var check = true;
        var message = '';
        var pli = false;
        var cards = data.cards

        for(let card of cards){
            card.value = parseInt(card.value)
        }

        if (partys[party_id].pile.player == getIndexOf(data.id, party_id) || (partys[party_id].rules["carreMagique"] && partys[party_id].pile.cards[partys[party_id].pile.cards.length-1].value == data.cards[0].value)){
            
            /*Regles de base*/
            
            //Fixe si on joue en simple, paire, triple, carré
            if (partys[party_id].pile.nb == 0){
                partys[party_id].pile.nb = cards.length;
            }
            
            //Verifie le nombre de carte posée
            if (cards.length != partys[party_id].pile.nb){
                message = 'Nombre de carte jouée incorrect';
                check = false;
            } else if (cards.length == 4){
                //TODO demander si le joueur souhaite appliqué la révo
            }

            let valueCard = cards[0].value;
            //Verifie que le joueur joue des cartes identiques
            for (let card of cards){
                if (card.value !== valueCard){
                    message = 'Cartes diférrentes';
                    check = false;
                }
            }

            //Verifie si il y a une révolution en cours
            if (!partys[party_id].pile.revo){
                //Verifie que le joueur joue au dessus
                if (valueCard < partys[party_id].pile.cards[partys[party_id].pile.cards.length-1].value){
                    message = 'Il faut jouer plus grand';
                    check = false;
                }
                //Verifie si c'est un 2
                if (valueCard == 15){
                    pli = true;
                    message = '2 posé';
                }
            }else{
                //Verifie que le joueur joue en dessous
                if (valueCard > partys[party_id].pile.cards[partys[party_id].pile.cards.length-1].value){
                    message = 'Il faut jouer plus petit';
                    check = false;
                }
                //Verifie si c'est un 3 en cas de revolution
                if (valueCard == 3){
                    pli = true;
                    message = '3 posé';
                }
            }

            //Verifie les carrés 
            let carre = true 
            if(partys[party_id].pile.cards.length > 4-cards.length){
                let i = (partys[party_id].pile.cards.length)-(4-cards.length)
                while(i<partys[party_id].pile.cards.length && carre){
                    if(partys[party_id].pile.cards[i].value!=cards[0].value){
                        carre = false
                    }
                    i++
                }
                if(carre){
                    pli = true;
                    message = 'Carré !';
                    io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('played', true)
                    partys[party_id].pile.player = getIndexOf(data.id, party_id)
                }else if(partys[party_id].pile.player != getIndexOf(data.id, party_id)){
                    check = false
                }
            }

            //TEST Active les TaGueule
            if (partys[party_id].pile.cards[partys[party_id].pile.cards.length-1].value == valueCard && !partys[party_id].pile.tg && partys[party_id].pile.nb == 1){
                partys[party_id].pile.tg = true;
                //message = cards[0].value+' ou rien !'
            }

            //TEST Vérifie les TaGueule
            if (partys[party_id].pile.cards[partys[party_id].pile.cards.length-1].value != valueCard && partys[party_id].pile.tg){
                check = false;
                partys[party_id].pile.tg = false;
            }

            /*Règles ajoutées*/

            //Verifie la règle 2toutSeul
            if (partys[party_id].rules["2toutSeul"]){
                if (valueCard == 2 && partys[party_id].pile.cards[0].value == 0 && !partys[party_id].pile.revo){
                    check = false;
                }
            }

            if(check && partys[party_id].players_end.length == partys[party_id].players.length-2 && partys[party_id].pile.round[partys[party_id].pile.player].nb_card-partys[party_id].pile.nb==0){
                attribuerRole(partys[party_id].players[partys[party_id].pile.player].id_user, party_id);
            }else if (check){
                //Si le joueur ne ferme le carré alors que ce n'est pas son tour, il est refusé
                if (partys[party_id].pile.player != getIndexOf(data.id, party_id) && partys[party_id].rules["carreMagique"] && !pli){
                    socket.emit('played',true);
                } else if ((partys[party_id].pile.player !== getIndexOf(data.id, party_id) && partys[party_id].rules["carreMagique"] && pli) || (partys[party_id].pile.player == getIndexOf(data.id, party_id))) { //TEST condition
                    //ajoute les cartes à la pile du serveur
                    for (let card of cards){
                        partys[party_id].pile.cards.push(card);
                    }
                    //Ajuste le nombre de carte du joueur
                    partys[party_id].pile.round[partys[party_id].pile.player].nb_card -= partys[party_id].pile.nb;
                    //Vérifie si il a fini, si oui lui attribut un role
                    if (partys[party_id].pile.round[partys[party_id].pile.player].nb_card == 0){
                        attribuerRole(partys[party_id].players[partys[party_id].pile.player].id_user, party_id);
                        message = partys[party_id].players[partys[party_id].pile.player].login+" n'a plus de carte !";
                        
                    }
                        //envoie une réponse aux joueurs
                        socket.emit('played',true, 'Bien joué');
                        io.in(partys[party_id].num).emit('update_pile', cards, message);
                        io.in(partys[party_id].num).emit('other_played', {login : partys[party_id].players[partys[party_id].pile.player].login, nb : partys[party_id].pile.nb}, me.login+' a joué '+partys[party_id].pile.nb+' "'+cards[0].value+'"');
                        //Si le jouer a fermé le carré
                        if (pli){
                            //Réinitialise la pile de jeu
                            partys[party_id].pile.nb = 0;
                            partys[party_id].players_pass = [];
                            partys[party_id].pile.tg = false;
                            if (!partys[party_id].pile.revo){
                                partys[party_id].pile.cards = [{value : 0, color : 'null'}];
                            } else{
                                partys[party_id].pile.cards = [{value : 16, color : 'null'}];
                            }
                            io.in(partys[party_id].num).emit('remove_pile', me.login+' a fini le tour avec '+cards.length+' "'+cards[0].value+'"');
                            //si le joueur a fait le pli on ne change pas de joueur
                            if (partys[party_id].pile.player !== getIndexOf(data.id, party_id)){ //cependant si il a utiisé la regle du carré magique oui
                                partys[party_id].pile.player = getIndexOf(data.id, party_id); //TEST
                            }
                            if (partys[party_id].pile.round[partys[party_id].pile.player].nb_card == 0){
                                nextPlayer(party_id);
                            }
                        } else {
                            //sinon on passe au joueur suivant
                            nextPlayer(party_id);
                        }
                        //fait jouer un joueur
                            io.to(partys[party_id].players[partys[party_id].pile.player].socket).emit('your_turn', partys[party_id].pile.tg);
                        
                }
            } else {
                socket.emit('played',false, 'Tu peux pas joué ca');
            }
        } else {
            socket.emit('played',false, "C'est pas à toi de jouer");
        }
    })


    /**
     * Déroulement partie coté serveur :
     *      1 ) groupe.emit('init_party', players) => initialisation position des joueurs et des autres éléments de jeu
     *          |_players : [{login : String},...] (ne pas mettre le login de l'utilisateur)
     * 
     *      2 ) groupe.emit('init_round', players) => initialise le nombres de cartes par joueur adverse
     *          |_players : [{login: String, nb_card: int, role: String or null}, ...] (ne pas mettre l'utilisateur)
     * 
     *      3 ) par_personne.emit('new_card', cards) => initialise les cartes du joueur
     *          |_cards : [{value : String, color: String}, ...] (ref. nom des fichiers cartes)
     * 
     *      4 ) une_personne.emit('your_turn', bool) => affiche les boutons : passe, je peux pas, le timer
     *          |_bool : True = affichage du bouton je peux pas jouer => 8 ou rien, ...
     *                   False = il ne peut que passer
     * 
     *      5 ) socket.on('play', function(cards){...}) => cartes que le joueur veut jouer
     *          |_cards: [{value : String, color: String}, ...] 
     *                                  ||
     *                                 \||/
     *                                  \/
     *          5.1) socket.emit('played', bool) => résultat de la vérification
     *               |_bool: true = cartes valide
     *                       false = cartes non valide
     * 
     *          if(validé){
     *              5.2 ) groupe.emit('update_pile', cards, msg) => maj de la pile
     *                  |_cards: [{value : String, color: String}, ...]
     *                  |_msg : String (etat de la pile : paire, 8 ou rien, ...)
     * 
     *              5.3 ) groupe.emit('other_played', player) => maj nb cartes d'un adversaire après qu'il est joué
     *                  |_player : {login: String, nb: int} (nb : nombres de cartes a supprimer au joueur)
     *          }
     * 
     *      6 ) socket.on('pass', function(){...}) => le joueur passe 
     * 
     *      
     */


    
}); 

function createDeck(){
    const valueArray = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]; 
    const colorArray = ["Trefle", "Carreau", "Coeur", "Pique"]; 

    var deck = [];

    for (i = 0; i < valueArray.length; i++) {
        for (j = 0; j < colorArray.length; j++) {
            deck.push({
                value : valueArray[i],
                color : colorArray[j]
            })
        }
    }
    var j, tmp;
    for (let i = deck.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        tmp = deck[i];
        deck[i] = deck[j];
        deck[j] = tmp;
    }
    return deck;
}

function distribution(playerNumber, id_party){
    var deck = createDeck();
    var handsArray = [];
    for (let i = 0; i < playerNumber; i++){
        handsArray.push([]);
    }
    for (let i = 0; i < deck.length; i++){
        handsArray[i%playerNumber].push(deck[i]);
        if (deck[i].value === 12 && deck[i].color === 'Coeur'){ 
            partys[id_party].pile.player = i%playerNumber; //Trouve le joueur avec la dame de coeur
        }
    }
    return handsArray;
}

function getIndexOf(id, party_id){
    for (let i = 0; i < partys[party_id].players.length; i++){
        if (partys[party_id].players[i].id_user === id) return i;
    }
}

function nextPlayer(party_id){
    let debut = partys[party_id].pile.player
    let change = false
    let i = 0

    while(!change){
        partys[party_id].pile.player = (partys[party_id].pile.player+1)%partys[party_id].players.length
        let user_id = partys[party_id].players[partys[party_id].pile.player].id_user
        if((!partys[party_id].players_pass.includes(user_id) && !partys[party_id].players_end.includes(user_id)) || (debut == partys[party_id].pile.player)){
            change = true
        }
    }
}

function attribuerRole(id, party_id){
    
    if (partys[party_id].players_end.length == 0){
        partys[party_id].role.President = id;
        partys[party_id].players_end.push(id);
    } else if (partys[party_id].players_end.length == 1){
        partys[party_id].role.Vice_President = id;
        partys[party_id].players_end.push(id);
    } else if (partys[party_id].players_end.length == partys[party_id].players.length-2){
        partys[party_id].role.Vice_Trou = id;
        partys[party_id].players_end.push(id);
        for(let player of partys[party_id].players){
            if (!partys[party_id].players_end.includes(player.id_user)){
                partys[party_id].role.Trou = player.id_user;
            }
        }

        io.to(partys[party_id].num).emit('end_game', partys[party_id])
        io.to(partys[party_id].num).emit('remove_pile')
        io.to(partys[party_id].players[getIndexOf(partys[party_id].master, party_id)].socket).emit('restart', partys[party_id])
    }else{
        partys[party_id].players_end.push(id);
    }
}


http.listen(80, function(req, res){
    console.log("server running");
});