var mysql = require('mysql')


class Model{
    constructor(host, user, password, db){
        this.connection = mysql.createConnection({
            host: host,
            user: user,
            password: password,
            database: db
          });

        this.connection.connect(function(err) {
            if (err) throw err;
            console.log("Connected!");
        });
    }

    /**
     * Vérifie si le pseudo est déja utilisé
     * 
     * Utilisation : - vérification lors de l'inscription   socket.on('sub', ... )
     *               - vérification lors de la connexion    socket.on('login', ...)
     * 
     * @param {String} login Pseudo a vérifier
     * @returns {function} Paramètre callback => true : le pseudo est déja utilisé
     *                                           false : le pseudo n'est pas encore utilisé
     */
    login_already_used(login, callback){
        var result = false;
        
        this.connection.query('Select * from User where login = ?',[login],(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                if(rows.length >= 1){
                    result = true
                }
                callback(result);
            }
            });
    
    };

    /**
     * Vérification que ce login n'est pas déja utilisé lors d'une modification de compte
     * 
     * @param {any} login 
     * @param {any} login_me 
     * @param {any} callback 
     * 
     * @memberOf Model
     */
    login_already_used_not_me(login, login_me, callback){
        
        if(login != login_me){
            this.login_already_used(login, callback)
        }else{
            callback(false)
        }

    };

    /**
     * Vérifie si l'email est déja utilisé
     * 
     * Utilisation : - vérification lors de l'inscription   socket.on('sub', ... )
     *               - vérification lors de la connexion    socket.on('login', ...)
     * 
     * @param {String} email Email a vérifier
     * @returns {function} callback => true : l'Email est déja utilisé
     *                                false : l'Email n'est pas encore utilisé
     *                                err : erreur de requete
     */
    email_already_used(email, callback){
        var result = false;
        
        this.connection.query('Select * from User where email = ?',[email],(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                if(rows.length >= 1){
                    result = true
                }
                callback(result);
            }
            });
    
    };

    email_already_used_not_me(email, email_me, callback){
        
        if(email != email_me){
            this.email_already_used(email, callback)
        }else{
            callback(false)
        }
        
    };

    change_user(data, callback){        
        this.connection.query('Update User set login = ?, email = ?, password = ? where id_user = ?',data, (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(true)
            }
        })
    }

    new_register(data, callback){

        this.connection.query("Insert into User (login, email, password) values ?",[[data]],(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(true)
            }
        });

    }


    /**
     * obtenir le mot de passe de ce login
     * 
     * Utilisation : - vérification lors de la connexion    socket.on('login', ...)
     * 
     * @param {String} login Pseudo de l'utilisateur qui veut s'authentifier
     * @returns {function} callback => password : mots de passe reliè a ce login
     *                                 err : problème de bdd
     */
    get_password(login, callback){

        this.connection.query("select password from User where login = ?",login,(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(rows[0].password);
            }
        });

    }

    /**
     * Renvoie les données de l'utilisateur ayant ce pseudo
     * 
     * Utilisation : - une fois la connexion effectuée                  socket.on('login', ... )
     *               - lors d'une demande d'amis pendant la recherche   socket.on('search', ... )     
     * 
     * @param {String} login Pseudo de l'utilisateur rechercher
     * @returns {Object:USER} Données de l'utilisateur rechercher
     */
    get_user_by_login(login, callback){
        this.connection.query("select * from User where login = ?",login,(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(rows[0]);
            }
        });
    }

    /**
     * Renvoie les ids des amis de l'utilisateur
     * 
     * Utilisation : - enregistrement des amis dans un utilisateur       socket.on('login', ...)
     * 
     * @param {Integer} id_user Id de l'utilisateur cherchant ses amis
     * @returns {function} callback => Tableau d'id des amis de l'utilisateur
     */
    get_ids_friend(id_user, callback){
        var res = []
        this.connection.query("select id_user1 from Friend where id_user2 = ?",id_user,(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                for(var row of rows){
                    res.push(row.id_user1)
                }
                this.connection.query("select id_user2 from Friend where id_user1 = ?",id_user,(err, rows, fields)=>{
                    if(err){
                        callback(err)
                    }else{
                        for(var row of rows){
                            res.push(row.id_user2)
                        }
                        callback(res)
                    }
                });
            }
        });
    }

    /**
     * Renvoie les données de l'utilisateur ayant cet Id
     * 
     * Utilisation : - affichage des données des amis         get_friends(id_friends)
     *               - affichage des demandes d'amis          get_requests(id_user)     
     * 
     * @param {Array:int} ids_user des l'utilisateurs rechercher
     * @returns {Object:USER} Données des l'utilisateurs rechercher
     */
    get_users(ids_user, callback){
        if(ids_user.length>0){
            var query = 'Select id_user, login, score from User where id_user = ? '
            var or = 'or id_user = ? '
            this.connection.query(query+(or.repeat(ids_user.length-1)), ids_user, (err, rows, fields)=>{
                if(err){
                    callback(err)
                }else{
                    callback(rows)
                }
            })
        }else{
            callback([])
        }
    }

    /**
     * Renvoie les données des utilisateurs voulant devenir des amis avec l'utilisateur 
     * 
     * Utilisation : - connexion de l'utilisateur socket.on('login', ... )
     * 
     * @param {integer} id_user Id de l'utilisateur conserné par les demande d'amis
     * @returns {function} callback qui prend en paramètre le tableau d'ID des demandeurs ou une erreur
     */
    get_requests(id_user, callback){
        var res = []
        this.connection.query('Select id_sender from FriendRequest where id_receiver = ?', id_user, (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                if(rows!=undefined && rows.length>0){
                    for(var row of rows){
                        res.push(row.id_sender)
                    }
                    this.get_users(res, callback)
                }else{
                    callback([])
                }
            }
        })
    }

    new_request(id_receiver, id_me, callback){
        this.connection.query("Insert into FriendRequest (id_receiver, id_sender) values ?",[[[id_receiver, id_me]]],(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(true)
            }
        })
    }

    /**
     * Vérifie si une demande d'amis n'a pas déja été envoyé par l'un des deux utilisateurs
     * 
     * Utilisation : - recherche d'amis     socket.on('search', ... )
     * 
     * @param {String} id_friend Id de l'amis rechercher
     * @param {integer} id_user id du chercheur
     * @returns {boolean} true : il y a déja une demande en cours
     *                    false : il n'y a pas de demande en cours
     */
    already_request(id_me, id_search, callback){
        this.connection.query('Select * from FriendRequest where (id_sender = ? and id_receiver = ?) or (id_sender = ? and id_receiver = ?)', [id_me, id_search, id_search, id_me], (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                if(rows!=undefined && rows.length>0){
                    callback(true)
                }else{
                    callback(false)
                }
            }
        })
    }

    save_user(data){
        this.connection.query('Update User set login = ?, email = ?, score = ?, number_of_party = ?, play_time = ? where id_user = ?',data, (err, rows, fields)=>{})
    }

    /**
     * Supprime le lien d'amitier entre 2 utilisateurs
     * 
     * Utilisation : - suppresion d'amitié      socket.on('friend_suppr', ... )
     * 
     * @param {integer} id_user1 Id d'un des amis 
     * @param {integer} id_user2 Id d'un des amis 
     * @returns {function} prend le true comme paramètre si la requète a bien été exécuté sinon l'erreur
     */
    delete_friendship(id_user1, id_user2, callback){
        this.connection.query('Delete from Friend where (id_user1 = ? and id_user2 = ?) or (id_user1 = ? and id_user2 = ?)', [id_user1, id_user2, id_user2, id_user1], (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(true)
            }
        })
    }


    /**
     * Supprime une demande d'amis spécifique
     * 
     * Utilisation : - acceptation d'une demande d'amis     socket.on('request_accept', ... )
     *               - refu d'une demande d'amis            socket.on('request_refuse', ... )
     * 
     * @param {integer} id_receiver Id de l'utilisateur qui a recu la demande
     * @param {integer} id_sender Id de l'utilisateur qui a envoyé la demande
     * @returns {function} prend le true comme paramètre si la requète a bien été exécuté sinon l'erreur
     */
    delete_request(id_receiver, id_sender, callback){

        this.connection.query('Delete from FriendRequest where id_receiver = ? and id_sender = ?', [id_receiver, id_sender], (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                callback(true)
            }
        })
    }

    new_friendship(id_me, id_user, callback){
        this.connection.query("Insert into Friend (id_user1, id_user2) values ?",[[[id_me, id_user]]],(err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                this.delete_request(id_me, id_user, callback)
            }
        })
    }

    get_search(login, id_me, callback){
        this.connection.query('Select id_user, login, score from User where UPPER(login) like UPPER( ? ) and not id_user = ?', ['%'+login+'%', id_me], (err, rows, fields)=>{
            if(err){
                callback(err)
            }else{
                if(rows!=undefined && rows.length>0){
                    callback(rows)
                }else{
                    callback(false)
                }
            }
        })
    }
}


module.exports = new Model('db', 'president', 'tr0u-d.K!', 'president')