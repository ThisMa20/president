(function($){

    Array.prototype.remove = function(from, to) {
        var rest = this.slice((to || from) + 1 || this.length);
        this.length = from < 0 ? this.length + from : from;
        return this.push.apply(this, rest);
    };    

    if(window.innerHeight>window.innerWidth){
        $('#err_screan').css('display','inline')
    }else{
        $('#intro').css('display','inline')
    }

    //var socket = io.connect('//localhost:8081');
    var socket = io.connect('//president.bigpapoo.com');

    $('.js_link_create').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#create_game').css('display','inline')
        $('.rule_magique_square_display').css('display','none')
        $('.rule_card_or_nothing_display').css('display','none')
        $('.collapseR1').css('display','none')
        $('.collapseR2').css('display','none')
        $('.collapseR3').css('display','none')
        $('.collapseR4').css('display','none')
        $('.collapseR5').css('display','none')
        $('.collapseR6').css('display','none')
        $('.collapseR7').css('display','none')
        $('.collapseR8').css('display','none')
    })

    $('.js_link_connection').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#connection').css('display','inline')
    })

    $('.js_link_join').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')

        socket.emit('load_join_party')

        game_request = 0
        $('.js_link_join a')[0].textContent = ''
        $('#join_party').css('display','inline')
    })

    $('.js_link_account').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')

        socket.emit('load_page_account')

        $('#account').css('display','inline')
    })

    $('.js_link_rules').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#rules').css('display','inline')
    })

    $('.js_link_table').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#table').css('display','inline')
    })

    $('.js_link_intro').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#intro').css('display','inline')
    })

    $('.js_link_register').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#register').css('display','inline')
    })
    
    $('.js_link_friend').submit(function(event){
        event.preventDefault()

        nb_request = 0

        $('#friend_info').empty()
        $('#friends').empty()
        $('#request_info').empty()
        $('#request').empty()
        $('#search_info').empty()
        $('#finded').empty()
        $('#friend_search_login').val('')
        $('.js_link_friend a').remove()
        $('.js_link_friend_request a').remove()

        socket.emit('load_page_friend')

        $('.js_page').css('display', 'none')
        $('#friend').css('display','inline')
    })

    $('.js_link_home').submit(function(event){
        event.preventDefault()

        socket.emit('load_page_home')

        $('.js_page').css('display', 'none')
        $('#home').css('display','inline')
    })
    
    $('.js_link_friend_request').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#friend_request').css('display','inline')
    })

    $('.js_link_friend_search').submit(function(event){
        event.preventDefault()
        $('.js_page').css('display', 'none')
        $('#friend_search').css('display','inline')
    })

    $('.js_disconnect').submit(function(event){
        event.preventDefault()
        socket.emit('disconnect')
        $('.js_page').css('display', 'none')
        $('#intro').css('display','inline')
    })

    socket.on('db_err', function(err_msg){
        alert(err_msg+"\n La requete que vous venez d'effectuer n'a pas aboutie\n Si l'erreur persiste merci de bien vouloir contacter le service client \n service.client@chichicorp.fr")
    })

////REGISTER PART//////////////////////////////////////////////////////

    $('#register_form').submit(function(event){
        event.preventDefault()
        socket.emit('register',{
            login : $('#register_login').val(),
            email : $('#register_email').val(),
            pwd1 : $('#register_pwd1').val(),
            pwd2 : $('#register_pwd2').val()
        })
    })

    socket.on('registered', function(){
        $('#register_info').empty()

        $('#connection_info').append('<p>Inscription reussi</p>')

        $('.js_page').css('display', 'none')
        $('#connection').css('display','inline')
    })

    socket.on('register_err', function(errs){
        $('#register_info').empty()

        for( var err of errs ){
            $('#register_info').append('<p>'+err+'</p>')
        }
    })      

////CONNECTION PART///////////////////////////////////////////////////////

    $('#connection_form').submit(function(event){
        event.preventDefault();
        
        socket.emit('try_to_connect',{
            login : $('#connection_login').val(),
            pwd : $('#connection_pwd').val()
        })
    })

    var me;

    socket.on('connected',function(data_me){
        $('#connection_info').empty()

        me = data_me

        $('.js_page').css('display', 'none')
        $('#home').css('display','inline')
    })

    socket.on('connection_err', function(errs){
        $('#connection_info').empty()

        for( var err of errs ){
            $('#connection_info').append('<p>'+err+'</p>')
        }
    })

    socket.on('back_to_connection', function(){
        $('.js_page').css('display', 'none')
        $('#connection').css('display','inline')

        $('#friends').empty()
        $('#request').empty()
        
        $('.js_link_friend a').remove()
        $('.js_link_friend_request a').remove()

        $('#account_score').html('<strong>Score</strong>')
        $('#account_time').html('<strong>Temps</strong>')
        $('#account_nb_party').html('<strong>Nb partie</strong>')
    })


///////FRIENDS////////////////////////////////////////////////////////

    socket.on('friend_load',function(friend){
        var txt = '<li id="friend_'+friend.login+'" class="alignement">Login : ' + friend.login + '&emsp;|&emsp;Score : ' + friend.score
        if(friend.socket){
            txt = txt +'&emsp;|&emsp;<a id="friend_connect_' +friend.login+'" style="color : green">•</a>'  
        }else{
            txt = txt +'&emsp;|&emsp;<a id="friend_connect_' +friend.login+'" style="color : red">•</a>'
        }
        txt = txt + '&emsp;<form action="" id="friend_'+friend.login+'_suppr"><button type="submit" name="Supprimer" title="Supprimer"><img src="icons/icons_supprimer.png"/></button></form></li>'

        $('#friends').append(txt)

        $('#friend_'+friend.login+'_suppr').submit(function(event){
            event.preventDefault()
            socket.emit('friend_suppr',friend)
        })

    })


    socket.on('friend_suppred',function(friend){
        $('#friend_info').append('<p>'+friend.login+" a été supprimé de votre liste d'amis </p>")
        $('#friend_'+friend.login).remove()
    })

    socket.on('friend_remove',function(friend_login){
        $('#friend_'+friend_login).remove()
    })

    socket.on('hello_my_friend',function(user){
        if(document.getElementById('friend_'+user.login) != null){
            document.getElementById('friend_connect_'+user.login).style.color = 'green';
        }
    })

    socket.on('goodbye_my_friend',function(user){
        if(document.getElementById('friend_'+user.login) != null){
            document.getElementById('friend_connect_'+user.login).style.color = 'red';
        }
    })

//////////FRIEND SEARCH//////////////////////////////////////////////

    $('#friend_search_form').submit(function(event){
        event.preventDefault()

        $('#finded').empty()

        socket.emit('search',$('#friend_search_login').val())

        $('#finded').empty()
    })

    socket.on('search_err',function(errs){

        $('#search_info').empty()
        
        for( var err of errs ){
            $('#search_info').append('<p>'+err+'</p>')
        }
    })

    socket.on('search_find', function(find){
        $('#search_info').empty()

        $('#finded').append("<div class='alignement'>Pseudo : " + find.login + "&emsp;|&emsp;Score : " + find.score + "&emsp;<form action='' id='find_"+find.login+"'><button type='submit' name='Ajouter' title='Ajouter'><img src='icons/icons_ajouter.png'/></button></form>")
        
        $('#find_'+find.login).submit(function(event){
            event.preventDefault()

            socket.emit('request_send', find)
        })
    })

    socket.on('request_sended', function(find){
        $('#find_'+find.login).remove()

        $('#search_info').append("<p>Votre demande d'amis a bien été envoyée à "+find.login+"</p>")
    })

//////////FRIEND REQUEST///////////////////////////////////////////

    var nb_request = 0

    socket.on('friend_request', function(applicant){
        $('#request').append("<div id='"+applicant.id_user+"' class='alignement'>Pseudo : " + applicant.login + "&emsp;|&emsp;Score : " + applicant.score + "&emsp;<form action='' id='"+applicant.id_user+"_accept'><button type='submit' name='Accepter' title='Accepter'><img src='icons/icons_valider.png'/></button></form><form action='' id='"+applicant.id_user+"_refuse'><button type='submit' name='Refuser' title='Refuser'><img src='icons/icons_refuser.png'/></button></form></div>")

        nb_request++
        if(nb_request>0){
            $('.js_link_friend a').remove()
            $('.js_link_friend_request a').remove()
        }
        $('.js_link_friend').append('<a>'+nb_request+'</a>')
        $('.js_link_friend_request').append('<a>'+nb_request+'</a>')
        
        $('#'+applicant.id_user+'_accept').submit(function(event){
            event.preventDefault()
            socket.emit('request_accept', applicant)
        })

        $('#'+applicant.id_user+'_refuse').submit(function(event){
            event.preventDefault()
            socket.emit('request_refuse', applicant)
        })
    })

    socket.on('friend_request_notif',function(){
        nb_request++
        if(nb_request>0){
            $('.js_link_friend a').remove()
            $('.js_link_friend_request a').remove()
        }
        $('.js_link_friend').append('<a>'+nb_request+'</a>')
        $('.js_link_friend_request').append('<a>'+nb_request+'</a>')
    })

    socket.on('request_accepted', function(applicant){
        $('#'+applicant.id_user).remove()

        $('#request_info').append('<p>'+applicant.login+" a été ajouté a votre liste d'amis </p>")

        nb_request--
        $('.js_link_friend a').remove()
        $('.js_link_friend_request a').remove()
        if(nb_request!=0){
            $('.js_link_friend').append('<a>'+nb_request+'</a>')
            $('.js_link_friend_request').append('<a>'+nb_request+'</a>')
        }
    })

    socket.on('request_refused', function(applicant){
        $('#'+applicant.id_user).remove()

        $('#request_info').append('<p>La requete de '+applicant.login+" a bien été refusé </p>")

        nb_request--
        $('.js_link_friend a').remove()
        $('.js_link_friend_request a').remove()
        if(nb_request!=0){
            $('.js_link_friend').append('<a>'+nb_request+'</a>')
            $('.js_link_friend_request').append('<a>'+nb_request+'</a>')
        }
    })

////////ACCOUNT//////////////////////////////////////////////////////////////////////

    socket.on('account_load', function(user){
        
        $('#account_score').text(' '+user.score)
        $('#account_time').text(' '+user.play_time)
        $('#account_nb_party').text(' '+user.number_of_party)

        $('#change_login').val(user.login)
        $('#change_email').val(user.email)
        
    })

    $('#change_form').submit(function(event){
        event.preventDefault()
        socket.emit('try_to_change', {login : $('#change_login').val(),
                                      email : $('#change_email').val(),
                                      pwd1  : $('#change_pwd1').val(),
                                      pwd2  : $('#change_pwd2').val()})
    })

    socket.on('change_err', function(errs){
        $('#change_info').empty()
        
        for( var err of errs ){
            $('#change_info').append('<p>'+err+'</p>')
        }
    })

    socket.on('change', function(data_me){
        me = data_me
    })
////////JOIN PARTY///////////////////////////////////////////////////////////////////

socket.on('load_join_party', function(partys){

    $('#waiting_party_list').empty()

    for(let id_party of Object.keys(partys)){
        let party = partys[id_party]
        
        $('#waiting_party_list').append('<li id="join_'+party.num+'">Partie n°'+party.num+' Nb joueurs : <a>'+party.players.length+'</a><form id="form_join_'+party.num+'"><input type="submit" value="Rejoindre"></form></li>')

        $('#form_join_'+party.num).submit(function(e){
            e.preventDefault()
            socket.emit('want_to_join', party.num)
        })
    }
})

socket.on('new_party', function(party){
    $('#waiting_party_list').append('<li id="join_'+party.num+'">Partie n°'+party.num+' Nb joueurs : <a>'+party.players.length+'</a><form id="form_join_'+party.num+'"><input type="submit" value="Rejoindre"></form></li>')

    $('#form_join_'+party.num).submit(function(e){
        e.preventDefault()
        socket.emit('want_to_join', party.num)
    })
})

socket.on('remove_join_party', function(party_id){
    $('#join_'+party_id).remove();
})

socket.on('update_join_party', function(party_id, nb_p_in){
    $('#join_'+party_id+' a')[0].textContent = nb_p_in;
})

socket.on('load_waiting_room', function(party){
    $('.js_page').css('display', 'none')
    $('#menu_waiting_room').empty()
    $('#menu_waiting_room').append('<form id="leave_waiting_room"><button type="submit" name="Accueil" title="Accueil"><img src="icons/icons_quitter.png" width=60px height=60px/></button></form>')
    $('#player_in_game').empty()
    $('#invite_player_in_game').css('display', 'none')
    $('#waiting_room').css('display','inline')

    for(let player of party.players ){
        $('#player_in_game').append('<li id="w_game_'+player.login+'">Login : ' + player.login + ' Score : ' + player.score+'</li>')
    }

    $('#leave_waiting_room').submit(function(e){
        e.preventDefault()
        if(confirm("Si vous quitter la salle d'attente vous ne serrez plus inscrit dans la partie")){
            socket.emit('want_to_leave', party.num)
            $('.js_page').css('display', 'none')
            $('#home').css('display','inline')
        }
    })
})

socket.on('master_leave', function(party_id){
    socket.emit('want_to_leave', party_id)
    alert('Le master a quitté la partie, elle a été annulé')
    $('.js_page').css('display', 'none')
    $('#home').css('display','inline')

    $('#hand_card').empty()
    $('#hand_bouton').empty()
    $('#other_player').empty()
    $('#hand_bouton').empty()
    $('#pile_msg').empty()
    $('#pile img').remove()
})

socket.on('new_player_join',function(player){
    $('#player_in_game').append('<li id="w_game_'+player.login+'">Pseudo : ' + player.login + ' Score : ' + player.score+'</li>')
})

socket.on('player_leave', function(player){
    $('#w_game_'+player.login).remove()
})

////////TABLE////////////////////////////////////////////////////////////////////////

var height_card_op, width_card_op, space_card_op, height_op, width_op

var height_card_me, width_card_me, space_card_me, aff_card_me 

var height_card_pile, width_card_pile, space_card_pile

function tri_cards(cards){
    let old_cards = cards
    let res = []
    
    let value = 3

    while(value<16){
        for(let card of old_cards){
            if(card.value==value){
                res.push(card)
            }
        }
        value++
    }

    return res
}

socket.on('new_cards', function(cards){

    var difference = space_card_me
    var card_larg = width_card_me
    var top_position = -aff_card_me
    var card_up = Math.round(height_card_me*0.2)

    var left_position = -Math.round((card_larg + (difference * (cards.length-1)))/2)

    cards = tri_cards(cards)

    for(var card of cards){
        $('#hand_card').append('<img id="'+card.value+'_'+card.color+'" src="cards/'+card.value+card.color+'.png" width="'+card_larg+'">')
        $('#'+card.value+'_'+card.color).css({'position': 'absolute', 'left': left_position, 'top': top_position})
        left_position += difference

        $('#'+card.value+'_'+card.color).hover(function(){
            if($(this).position().top == top_position){
                $(this).css('top', (top_position-card_up)) 
            } 
        }, function(){
            if($(this).position().top == top_position-card_up){
                $(this).css('top', (top_position))
            }
        })

        $('#'+card.value+'_'+card.color).click(function(){
            if($(this).position().top == top_position-card_up-1){
                $(this).css('top', (top_position))
                $(this).removeClass('js_selected')
            }else{
                $(this).css('top', (top_position-card_up-1))
                $(this).addClass('js_selected')
            }
        })

    }
    
})

socket.on('init_party', function(players){

    height_card_op = Math.round(window.innerHeight*0.15)
    width_card_op = Math.round(height_card_op*0.7)
    space_card_op = Math.round(width_card_op*0.15)
    height_op = height_card_op + 22
    width_op = Math.round(12 * space_card_op + width_card_op)

    height_card_me = Math.round(window.innerHeight*0.35)
    width_card_me = Math.round(height_card_me*0.7)
    space_card_me = Math.round(window.innerWidth*0.03)
    aff_card_me = Math.round(window.innerHeight*0.2)

    height_card_pile = Math.round(window.innerHeight*0.30)
    width_card_pile = Math.round(height_card_pile*0.7)
    space_card_pile = Math.round(width_card_pile*0.20)


    var space_player_top = Math.round(window.innerWidth*0.75/6)
    var position_player = [
        {position: 'absolute',left : Math.round(window.innerWidth*0.01 + width_op/2), top : Math.round(window.innerHeight/2 + height_op/2)},
        {position: 'absolute',left : Math.round(window.innerWidth*0.99 - width_op/2), top : Math.round(window.innerHeight/2 + height_op/2)},
        {position: 'absolute',left : Math.round(space_player_top + window.innerWidth*0.25/2) , top : height_op - 15},
        {position: 'absolute',left : Math.round(space_player_top*3 + window.innerWidth*0.25/2), top : height_op - 15},
        {position: 'absolute',left : Math.round(space_player_top*5 + window.innerWidth*0.25/2), top : height_op - 15}
    ]
    var i = 0
    for(var player of players){
        if (player!=me.login){
            $('#other_player').append('<div id="'+player+'_player"><div id="'+player+'_cards"></div><h3>'+player+'</h3></div>')
            $('#'+player+'_player').css(position_player[i])
            $('#'+player+'_player h3').css({position: 'absolute',left : -($('#'+player+'_player h3')[0].textContent.length*7)/2, top : -10})
            i++
        }
    }

    $('#hand_card').css({position : 'absolute', top : window.innerHeight, left : window.innerWidth/2})
    $('#msg_played').css({position : 'absolute', top: window.innerHeight-aff_card_me-50, left: window.innerWidth/2-($('#msg_played').width()/2)})
    
    $('#pile').css({position: 'absolute', top: window.innerHeight/2, left: window.innerWidth/2})
    $('#pile_msg').css({position: 'absolute', top: height_card_pile/2 + $('#pile_msg').height(), left: -100, 'text-align': 'left'})

    $('.js_page').css('display', 'none')
    $('#table').css('display', 'inline')
})

socket.on('init_round', function(players){
    var decalage = space_card_op
    var larg = width_card_op
    for(var player of players){
        if(player.login != me.login){
            var i = 0
            var position = 0
            $('#'+player.login+'_cards').css({position: 'absolute', left : -((player.nb_card-1)*decalage + larg)/2, top : -larg*1.5+($('#'+player.login+'_player h3').height()/2)})
            while(i<player.nb_card){
                $('#'+player.login+'_cards').append('<img src="cards/Vide.png" width="'+larg+'">')
                $('#'+player.login+'_cards img').last().css({position : 'absolute', left : position})
                position += decalage
                i++
            }
        }
    }
})


$(document).keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        var selected = $('.js_selected')
        var selected_id = []
        for(var i=0;i<selected.length;i++){
            var ids = $('.js_selected')[i].id.split('_')
            selected_id.push({value : ids[0], color : ids[1]})
        }
        socket.emit('play',{cards: selected_id, id: me.id_user})
    }
});

socket.on('update_pile', function(cards, msg){
    var difference = space_card_pile
    var card_larg = width_card_pile
    var top_position = -Math.round(height_card_pile/2)

    var left_position = -Math.round((card_larg + (difference * (cards.length-1)))/2)

    $('#pile_msg')[0].textContent = msg
    $('#pile_msg').css({left : -(msg.length*7)/2})

    for(var card of cards){
        $('#pile').append('<img id="'+card.value+'_'+card.color+'" src="cards/'+card.value+card.color+'.png" width="'+card_larg+'">')
        $('#'+card.value+'_'+card.color).css({'position': 'absolute', 'left': left_position, 'top': top_position})
        left_position += difference
    }
})

socket.on('other_played',function(player, msg){
    $('#pile_msg').text(msg)
    $('#pile_msg').css({left : -(msg.length*7)/2})

    var decalage = space_card_op
    var larg = width_card_op
    var i = 0
    while(i<player.nb){
        $('#'+player.login+'_cards img').last().remove()
        i++
    }
    $('#'+player.login+'_cards').css({position: 'absolute', left : -((($('#'+player.login+'_cards').children().length)-1)*decalage + larg)/2, top : -larg*1.5+($('#'+player.login+'_player h3').height()/2)})
})

socket.on('played', function(res, msg=' '){
    if(res){
        $('.js_selected').remove()
        $('#hand_bouton').empty()
    }
    $('#msg_played').text(msg)
    $('#msg_played').fadeIn()
    $('#msg_played').delay(3000).fadeOut()

    calcul_position_hand();
})

socket.on('remove_pile', function(msg){
    $('#pile_msg').text(msg)
    $('#pile_msg').css({left : -(msg.length*7)/2})

    $('#pile img').remove()
})
           
var calcul_position_hand = function(){
    $('.js_selected').removeClass('js_selected')
    var cards = $('#hand_card').children()

    var difference = space_card_me
    var card_larg = width_card_me
    var top_position = -aff_card_me

    var left_position = -Math.round((card_larg + (difference * (cards.length-1)))/2)

    for(var i=0;i<cards.length;i++){
        $('#hand_card').append(cards[i])
        $('#hand_card img:last-child').css({'left': left_position, 'top': top_position})
        left_position+=difference
    }

}

socket.on('your_turn', function(bool){
    var nb_card = Math.round(54/($('#other_player').children().length+1))
    var difference = space_card_me
    var card_larg = width_card_me
    var h_c = aff_card_me

    var right_position_card = $('#hand_card').position().left + Math.round((card_larg + (difference * (nb_card-1)))/2)

    $('#hand_bouton').css({position : 'absolute', left : Math.round(right_position_card + window.innerWidth*0.03), top: window.innerHeight})

    $('#hand_bouton').append('<form action="" id="hand_pass"><input type="submit" value="Passe"></form>')

/*
    //src countdown : https://codepen.io/zebateira/pen/VvqJwm
    $('#hand_bouton').append('<div id="countdown"><div id="countdown-number"></div><svg><circle r="18" cx="20" cy="20"></circle></svg></div>')

    $('#countdown').css({top:-h_c, left :Math.round(window.innerWidth*0.05-20)})
    $('#svg').css({top:-h_c})*/
    $('#hand_pass input').css({position:'absolute', top:-Math.round(h_c - h_c*0.5), height : Math.round(h_c*0.2), width: Math.round(window.innerWidth*0.1)})

    

    $('#hand_pass').submit(function(e){
        e.preventDefault()
        calcul_position_hand();
        socket.emit('pass')
    })

    if(bool){
        $('#hand_bouton').append('<form action="" id="hand_cant"><input type="submit" value="J\'peux pas"></form>')
        $('#hand_cant input').css({position:'absolute', top:-Math.round(h_c - h_c*0.75), height : Math.round(h_c*0.2), width: Math.round(window.innerWidth*0.1)})
    
        $('#hand_cant').submit(function(e){
            e.preventDefault()
            calcul_position_hand();
            socket.emit('play', null)
        })
    
    }

    /*
    var countdownNumberEl = document.getElementById('countdown-number');
    var countdown = 30;

    countdownNumberEl.textContent = countdown;

    setInterval(function() {
        if($('#hand_bouton').children().length == 0){
            if(countdown>1){
                countdown--
                countdownNumberEl.textContent = countdown;
            }else{
                countdown--
                countdownNumberEl.textContent = countdown;
                calcul_position_hand();
                socket.emit('pass')
                return;
            }
        }else{
            return;
        }
    }, 1000);*/
})

/////END GAME//////////////////////////////////////

socket.on('end_game',function(party){

    $('#table').css({display:'none'})

    $('#hand_card').empty()
    $('#hand_bouton').empty()
    $('#other_player').empty()
    

    $('#end_game').css({display:'inline'})
    $('#list_winner').empty()
    for(let position in party.role){
        let txt = '<li>'+position +' : '
        let i = 0
        let trouve = false

        while(!trouve){
            if(party.players[i].id_user == party.role[position]){
                let player = party.players[i]
                trouve = true
                txt += player.login+' Score : '+player.score+'</li>'
            }
            i++
        }

        $('#list_winner').append(txt)
    }
})

socket.on('restart', function(party){
    if($('#form_restart').length == 1){
        $('#form_restart').remove()
    }
    $('#end_game').append('<form id="form_restart"><input type="submit" value="C\'est repartie"></form>')

    $('#form_restart').submit(function(e){
        e.preventDefault()
        socket.emit('want_to_start', party.num)
    })
})

/////CREATE GAME///////////////////////////////////

var rules_selected = [];

$('.rule_card_or_nothing_class').click(function(event){
    $(this).siblings('input:checkbox.rule_card_or_nothing_class').prop('checked', false);
    if($('.rule_card_or_nothing_class:checked').attr('id')==null)
    {
        $('#rule_card_or_nothing_1').prop('checked', true);
    }
})

$('.rule_magic_square_class').click(function(event){
    $(this).siblings('input:checkbox.rule_magic_square_class').prop('checked', false);
    if($('.rule_magic_square_class:checked').attr('id')==null)
    {
        $('#rule_magic_square_1').prop('checked', true);
    }
})

$('#rule_magic_square').click(function(event){
    if(($('#rule_magic_square:checked').attr('id')==null))
    {
        $('.rule_magique_square_display').css('display','none')
    }else
    {
        $('.rule_magique_square_display').css('display','inline')
    }
})

$('#rule_card_or_nothing').click(function(event){
    if(($('#rule_card_or_nothing:checked').attr('id')==null))
    {
        $('.rule_card_or_nothing_display').css('display','none')
    }else
    {
        $('.rule_card_or_nothing_display').css('display','inline')
    }
})

$('#rules_form').submit(function(event){
    event.preventDefault()
    rules_selected = {}

    $('.rule:checked').each(function() {
        rules_selected[$(this).attr('id')] = true;
    });
   
    /*
    if(rules_selected.includes("rule_card_or_nothing"))
    {
       rules_selected.push($('.rule_card_or_nothing_class:checked').attr('id'))
       rules_selected.splice(rules_selected.indexOf('rule_card_or_nothing'),1)
    }

    if(rules_selected.includes("rule_magic_square"))
    {
        rules_selected.push($('.rule_magic_square_class:checked').attr('id'))
        rules_selected.splice(rules_selected.indexOf('rule_magic_square'),1)
    }*/

    //console.log(rules_selected)
    
    socket.emit('rules', rules_selected)
    
})

socket.on('rules_accepted',function(party_id,me){
    $('.js_page').css('display', 'none')
    $('#friend_invite_in_game').empty()
    $('#player_in_game').empty()

    $('#player_in_game').append('<li id="w_game_'+me.login+'">Login : ' + me.login + ' Score : ' + me.score+'</li>')
    socket.emit('load_friends_list_in_game', party_id)

    $('#waiting_room #start').empty()
    $('#waiting_room #start').append('<form id="start_game_'+party_id+'"><input type="submit" value="Start"></form>')

    $('#waiting_room').css('display','inline')

    $('#start_game_'+party_id).submit(function(e){
        e.preventDefault()
        socket.emit('want_to_start', party_id)
    })

    $('#menu_waiting_room').empty()
    $('#menu_waiting_room').append('<form id="leave_waiting_room"><button type="submit" name="Accueil" title="Accueil"><img src="icons/icons_quitter.png" width=60px height=60px/></button></form>')

    $('#leave_waiting_room').submit(function(e){
            e.preventDefault()
            if(confirm("Si vous quitter la salle d'attente votre partie sera detruite")){
                socket.emit('want_to_leave', party_id)
                $('.js_page').css('display', 'none')
                $('#home').css('display','inline')
            }
    })

})

socket.on('friend_load_to_invite_in_game',function(friend, party_id){
    var txt = '<li id="a_friend_'+friend.login+'">Login : ' + friend.login + ' Score : ' + friend.score

    txt = txt + '<form action="" id="friend_'+friend.login+'_invite_in_game"><input type="submit" value="Inviter"></form></li>'

    $('#friend_invite_in_game').append(txt)

    $('#friend_'+friend.login+'_invite_in_game').submit(function(event){
        event.preventDefault()
        socket.emit('game_request_send',friend, party_id)
        $('#friend_info_invite_in_game').append('<p>'+friend.login+" a été invité dans votre partie </p>").delay(3000).remove()
        $('#friend_'+friend.login+'_invite_in_game').remove()
    })

})

socket.on('start_game_err', function(txt){
    $('#end_info').empty()
    $('#end_info').append('<p>'+txt+'</p>')
    $('#w_room_info').empty()
    $('#w_room_info').append('<p>'+txt+'</p>')
})

var game_request = 0

socket.on('game_request', function(party){

    game_request++;

    $('.js_link_join a')[0].textContent = game_request
    
    $('#invitation_list').append('<li id="join_'+party.num+'">Partie n°'+party.num+' Nb joueurs : <a>'+party.players.length+'</a><form id="form_r_join_'+party.num+'"><input type="submit" value="Rejoindre"></form></li>')

    $('#form_r_join_'+party.num).submit(function(event){
        event.preventDefault()
        socket.emit('want_to_join', party.num) //TODO
        
       /*TODO Détecter sur quel page il est puis enlever tout les affichages de cette page et afficher la page de la création de partie  + Le rajouter à la liste des joueurs de la partie (variable côté serveur)*/
        
    })
})


R1_display = false;
R2_display = false;
R3_display = false;
R4_display = false;
R5_display = false;
R6_display = false;
R7_display = false;
R8_display = false;

$('#R1').click(function(event){
    event.preventDefault()
    if(R1_display){
        $('.collapseR1').css('display','none')
        R1_display = false
    } else
    {
        $('.collapseR1').css('display','inline')
        R1_display = true
    }
})

$('#R2').click(function(event){
    event.preventDefault()
    if(R2_display){
        $('.collapseR2').css('display','none')
        R2_display = false
    } else
    {
        $('.collapseR2').css('display','inline')
        R2_display = true
    }      
})

$('#R3').click(function(event){
    event.preventDefault()
    if(R3_display){
        $('.collapseR3').css('display','none')
        R3_display = false
    } else
    {
        $('.collapseR3').css('display','inline')
        R3_display = true
    }      
})

$('#R4').click(function(event){
    event.preventDefault()
    if(R4_display){
        $('.collapseR4').css('display','none')
        R4_display = false
    } else
    {
        $('.collapseR4').css('display','inline')
        R4_display = true
    }      
})

$('#R5').click(function(event){
    event.preventDefault()
    if(R5_display){
        $('.collapseR5').css('display','none')
        R5_display = false
    } else
    {
        $('.collapseR5').css('display','inline')
        R5_display = true
    }      
})

$('#R6').click(function(event){
    event.preventDefault()
    if(R6_display){
        $('.collapseR6').css('display','none')
        R6_display = false
    } else
    {
        $('.collapseR6').css('display','inline')
        R6_display = true
    }      
})

$('#R7').click(function(event){
    event.preventDefault()
    if(R7_display){
        $('.collapseR7').css('display','none')
        R7_display = false
    } else
    {
        $('.collapseR7').css('display','inline')
        R7_display = true
    }      
})

$('#R8').click(function(event){
    event.preventDefault()
    if(R8_display){
        $('.collapseR8').css('display','none')
        R8_display = false
    } else
    {
        $('.collapseR8').css('display','inline')
        R8_display = true
    }      
})

})(jQuery);